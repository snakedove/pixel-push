if (typeof $ !== 'undefined' && typeof $.fn !== 'undefined') {
    var prevPos = {
        x: 0,
        y: 0
    };
    var xDiff = 0;
    var yDiff = 0;
    var colors = ['red', 'yellow', 'blue', 'purple', 'orange', 'green'];

    $.fn.pixelPush = function (options) {
        if (!$(this).length) {
            return;
        }

        $.each($(this), function (index, el) {
            var posTop = !options || typeof options.top === 'undefined' ? 10 : options.top;
            var posLeft = !options || typeof options.left === 'undefined' ? 10 : options.left;
            var size = !options || !options.size ? 10 : options.size;
            var speed = !options || !options.speed ? 10 : options.speed;
            var growth = !options || !options.growthFactor ? 10 : options.growthFactor;
            var changeColors = !options || !options.changeColors ? 10 : options.changeColors;

            $(el).attr('data-top', posTop);
            $(el).attr('data-left', posLeft);
            $(el).attr('data-size', size);
            $(el).attr('data-color', $(el).css('background-color'));
            $(el).css({
                'top': posTop + 'px',
                'left': posLeft + 'px'
            });
            $(el).on('mouseenter', function (e) {
                var cssOpt = {
                    'top': (posTop + (yDiff * speed)) + 'px',
                    'left': (posLeft + (xDiff * speed)) + 'px',
                    'width': (parseInt($(el).css('width').replace('px', '')) * growth) + 'px',
                    'height': (parseInt($(el).css('height').replace('px', '')) * growth) + 'px',
                    'transition': 'top 1s, left 1s, width 1s, height 1s, background-color 2s'
                };
                if (changeColors) {
                     cssOpt['background-color'] = colors[Math.floor(Math.random() * colors.length)];
                }
                $(e.target).css(cssOpt);
            });
        });
    };

    $(document).on('mousemove touchmove', function (e) {
        var pageX = e.pageX;
        var pageY = e.pageY;

        if (e.type === 'touchmove') {
            pageX = e.touches[0].pageX;
            pageY = e.touches[0].pageY;
        }

        xDiff = pageX - prevPos.x;
        yDiff = pageY - prevPos.y;
        prevPos.x = pageX;
        prevPos.y = pageY;

        if (e.type === 'touchmove') {
            var $el = $(document.elementFromPoint(pageX, pageY));
            if ($el.length && $el.hasClass('dot')) {
                $el.trigger('mouseenter');
            }
        }

    });

    $(document).on('click touchend', function () {
        $.each($('.dot'), function (key, value) {
            $(value).css({
                'top': $(value).attr('data-top') + 'px',
                'left': $(value).attr('data-left') + 'px',
                'height': $(value).attr('data-size') + 'px',
                'width': $(value).attr('data-size') + 'px',
                'background-color': $(value).attr('data-color'),
            });
        });
    });
}
