# README #

### Pixel Push ###

* animate your blocks and shift them around
* supports tablet devices
* Version 1.0

### How do I get set up? ###

* needs jQuery
* options: speed (default: 10), size (default: 10), top, left, growthFactor, changeColors
* use: $(element).pixelPush(options);
* element needs to be absolute positioned relative to its parent
* move mouse or touch-swipe
* clicking somewhere resets to original

example here:
http://www.haraldbauhofer.de